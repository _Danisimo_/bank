public class BankOperation {

    public static final int BALANCE = 50000;
    public static final int MAX_BALANCE = 1000000;

    public static final int MAX_CREDIT = 20000;
    public static final int MIN_CREDIT = 500;

    public static final int MAX_DEPOSIT = 20000;
    public static final int MIN_DEPOSIT = 1000;

    public static final int MAX_DEPOSIT_TERM =120;
    public static final int MIN_DEPOSIT_TERM =360;

    private double minBalance;
    private double balance;

    private double deposite;
    private int depositeTerm;
    private double sumOfDeposite;

    private double credit;
    private int creditTerm;
    private double sumOfCredit;


    public void bankDeposite(double deposite, int depositeTerm){
        deposite = MIN_DEPOSIT + (int) (Math.random() * (MAX_DEPOSIT +1));
        depositeTerm = MIN_DEPOSIT_TERM + (int) (Math.random() * (MAX_DEPOSIT_TERM +1));

        sumOfDeposite += deposite;
    }

    public void bankCredit(double credit, int creditTerm)
    {
        credit = MIN_CREDIT + (int) (Math.random() * (MAX_CREDIT +1));
        //creditTerm =MIN_CREDIT + (int) (Math.random() * (MAX_CREDIT +1));
        sumOfCredit +=credit;
    }




}
